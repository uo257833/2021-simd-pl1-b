
 /* Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>

using namespace cimg_library;

//times that we repeat the algorithm to get meaningful times (between 5-10 seconds)
const int loops=20;

// Data type for image components
typedef float data_t;
#define ITEMS_PER_PACKET (sizeof(__m128)/sizeof(data_t))

const char* SOURCE_IMG      = "bailarina.bmp"; //first image
const char* DESTINATION_IMG = "bailarina2.bmp"; //new image generated
const char* SOURCE_IMG_2    = "flores_3.bmp"; //second image

int main() {

	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> srcImage2(SOURCE_IMG_2);

	//test if the images exist
	if (srcImage==NULL){
		printf("First image does not exist");
		return 0;
	}
	if (srcImage2==NULL){
		printf("Second image does not exist");
		return 0;
	}
	
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of the first image (X)
	data_t *pRsrc2, *pGsrc2, *pBsrc2; // Pointers to the R, G and B components of the second image (Y)
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image. Both images must be equals
	uint nComp; // Number of image components
    uint index;

	struct timespec tStart, tEnd;
	double dElapsedTimeS;
	float Rt, Gt, Bt;
	//modifiers of the algorythm
	Rt = 0.4; //red modifier
	Gt = 0.1; //green modifier
	Bt = 0.2; //blue modifier

	//test if the images are the same size (height and weight must be equals)
	if (srcImage.width()!=srcImage2.width() || srcImage.height()!=srcImage2.height()){
		perror("images are not equal size");
		exit (-2);
	}

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	srcImage2.display(); // Displays the source image

	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image (X)
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the componet arrays of the second source image (Y)
	pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
	pGsrc2 = pRsrc2 + height * width; // pGcomp points to the G component array
	pBsrc2 = pGsrc2 + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	//start measuring time
	if (clock_gettime(CLOCK_REALTIME, &tStart)<0){
		perror("error while starting time measure");
		exit (-2);
	}

//ALGORITHM
//	    R = R * Rt
//		G = G * Gt
//		B = B * Bt
	//pdest --> pixel of destination image(new image)
	//psrc --> pixel of the image we modify
	__m128 vConst=_mm_set1_ps((float)256);
	__m128 vConst2=_mm_set1_ps(1);
	__m128 vRt=_mm_set1_ps((float)Rt);
	__m128 vGt=_mm_set1_ps((float)Gt);
	__m128 vBt=_mm_set1_ps((float)Bt);
	for (uint j=0;j<loops;j++){

		for (uint i=0; i<height;i++){
            for(uint j=0; j<width; j=j+ITEMS_PER_PACKET){
                        index= i*width + j;
			//pRdest[i]= (256 * (pRsrc2[i] * Rt)) / ((pRsrc[i] * Rt) + 1 );
            //Calculate R
            __m128 vX=_mm_loadu_ps(pRsrc + index);
			__m128 vY=_mm_loadu_ps(pRsrc2 + index);
			vX=_mm_mul_ps(vX,vRt);
			vY=_mm_mul_ps(vY,vRt);
            //a= 256 x Y
            __m128 aR=_mm_mul_ps(vConst,vY);
            //b=1+X
            __m128 bR=_mm_add_ps(vConst2,vX);
            //result=a/b
            __m128 resultR=_mm_div_ps(aR,bR);
            _mm_storeu_ps((pRdest + index),resultR); //to save
			
            //pGdest[i]= (256 * (pGsrc2[i] * Gt)) / ((pGsrc[i] * Gt) + 1 );
            
            //Calculate G
            vX=_mm_loadu_ps(pGsrc + index);
            vY=_mm_loadu_ps(pGsrc2 + index);
			vX=_mm_mul_ps(vX,vGt);
			vY=_mm_mul_ps(vY, vGt);
            //a= 256 x Y
            __m128 aG=_mm_mul_ps(vConst, vY);
            //b=1+X
            __m128 bG=_mm_add_ps(vConst2,vX);
            //result=a/b
            __m128 resultG=_mm_div_ps(aG, bG);
            _mm_storeu_ps((pGdest + index), resultG); //to save
			
            //pBdest[i]= (256 * (pBsrc2[i] * Bt)) / ((pBsrc[i] * Bt) + 1 );		
            
            //Calculate B
            vX=_mm_loadu_ps(pBsrc + index);
            vY=_mm_loadu_ps(pBsrc2 + index);
			vX=_mm_mul_ps(vX,vBt);
			vY=_mm_mul_ps(vY, vBt);
            //a= 256 x Y
            __m128 aB=_mm_mul_ps(vConst, vY);
            //b=1+X
            __m128 bB=_mm_add_ps(vConst2,vX);
            //result=a/b
            __m128 resultB=_mm_div_ps(aR, bR);
            _mm_storeu_ps((pBdest + index), resultB); //to save
		    }
        }
	}

	if (clock_gettime(CLOCK_REALTIME, &tEnd)<0){ //stop measuring time
		perror("error at the end of time measuring");
		exit (-2);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) /1e+9;
		
	printf("\n\n\nTotal time %f\n\n\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.

	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	dstImage.cut(0,255); //this makes all pixels of the image to be between values 0 and 255 (if they are lower, values are set to 0 and higher to 255)
	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);
	return 0;
}
